using System;
using System.Collections;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerExplodeSound;
        [SerializeField] private float playerExplodeSoundVolume = 0.3f;
        
        [SerializeField] private Bullet ultiBullet;
        [SerializeField] private UltiCooldown ultiState;
        [SerializeField] private float coolDownTimer;

        [SerializeField] private Camera mainCamera;

        public event Action TakingHit;
        
        internal enum UltiCooldown
        {
            Idle,
            Cooldown,
        }
        private void Awake()
        {
            mainCamera = Camera.main;
            Debug.Assert(mainCamera != null,"mainCamera cannot be null");
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            ultiState = UltiCooldown.Idle;
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound,mainCamera.transform.position,playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, transform.rotation);
            bullet.Init(transform.up);
        }

        public void FiringUlti()
        {
            if (ultiState != UltiCooldown.Cooldown)
            {
                ultiState = UltiCooldown.Cooldown;
                AudioSource.PlayClipAtPoint(playerFireSound,mainCamera.transform.position,playerFireSoundVolume);
                var ulti = Instantiate(ultiBullet, gunPosition.position, transform.rotation);
                ulti.Init(transform.up);
                StartCoroutine(UltiCooldownCounter());
            }
        }

        private IEnumerator UltiCooldownCounter()
        {
            yield return new WaitForSeconds(coolDownTimer);
            ultiState = UltiCooldown.Idle;
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if (Hp > 0)
            {
                TakingHit?.Invoke();
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
            AudioSource.PlayClipAtPoint(playerExplodeSound, mainCamera.transform.position, playerExplodeSoundVolume);
        }
    }
}