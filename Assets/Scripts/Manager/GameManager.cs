﻿using System;
using System.Collections;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button endButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private RectTransform nextDialog;
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private EnemySpaceship bossSpaceShips;
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;

        [SerializeField] private int scoreGoal;

        public static GameManager Instance;

        private PlayerSpaceship playerPrefab;
        private EnemySpaceship enemyPrefabs;
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            
            if (Instance != this)
            {
                Destroy(this.gameObject);
            }
            
            DontDestroyOnLoad(Instance);
            
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            dialog.gameObject.SetActive(true);
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            nextButton.onClick.AddListener(OnNextButtonClicked);
            endButton.onClick.AddListener(OnEndButtonClicked);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void OnNextButtonClicked()
        {
            nextDialog.gameObject.SetActive(false);
            StartCoroutine(LoadScene());
        }

        private IEnumerator LoadScene()
        {
            yield return SceneManager.LoadSceneAsync("Scenes/Boss Scene");
            SpawnPlayerSpaceship();
            SpawnBossSpaceship();
        }

        private void OnEndButtonClicked()
        {
            Application.Quit();
        }
        
        private void StartGame()
        {
            ScoreManager.Instance.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
        }
        
        private void SpawnPlayerSpaceship()
        {
            playerPrefab = Instantiate(playerSpaceship);
            playerPrefab.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            playerPrefab.OnExploded += OnPlayerSpaceshipExploded;
            playerPrefab.TakingHit += OnPlayerTakingHit;
            HealthControl.Instance.SetMaxHealth(playerSpaceshipHp,playerPrefab.Hp);
        }

        private void OnPlayerTakingHit()
        {
            playerPrefab.TakingHit -= OnPlayerTakingHit;
            HealthControl.Instance.SetHealth(playerPrefab.Hp);
            playerPrefab.TakingHit += OnPlayerTakingHit;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            enemyPrefabs = Instantiate(enemySpaceship);
            enemyPrefabs.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed,playerPrefab.transform);
            enemyPrefabs.OnExploded += OnEnemySpaceshipExploded;
        }

        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.AddScore(1);
            if (ScoreManager.Instance.GetScore() < scoreGoal)
            {
                StartCoroutine(SpawningEnemy());
                return;
            }
            DestroyRemainingShips();
            NextScene();
        }

        private IEnumerator SpawningEnemy()
        {
            yield return new WaitForSeconds(1);
            SpawnEnemySpaceship();
        }
        
        private void SpawnBossSpaceship()
        {
            enemyPrefabs = Instantiate(bossSpaceShips);
            enemyPrefabs.Init(enemySpaceshipHp * 2, enemySpaceshipMoveSpeed * 2,playerPrefab.transform);
            enemyPrefabs.OnExploded += OnBossSpaceshipExploded;
        }

        private void OnBossSpaceshipExploded()
        {
            ScoreManager.Instance.AddScore(10);
            DestroyRemainingShips();
            EndScene();
        }
        
        private void Restart()
        {
            DestroyRemainingShips();
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }

        private void NextScene()
        {
            nextDialog.gameObject.SetActive(true);
        }

        private void EndScene()
        {
            endDialog.gameObject.SetActive(true);
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }

            var remainingPlayer = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayer)
            {
                Destroy(player);
            }
        }

    }
}
