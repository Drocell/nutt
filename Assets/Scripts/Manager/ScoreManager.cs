﻿using System.ComponentModel.Design.Serialization;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;

        private int playerScore;

        public static ScoreManager Instance;
        
        public void Init()
        {
            GameManager.Instance.OnRestarted += OnRestarted;
            finalScoreText.gameObject.SetActive(false);
            HideScore(false);
            SetScore();
        }

        public void ResetScore()
        {
            playerScore = 0;
            SetScore();
        }

        public void AddScore(int score)
        {
            playerScore += score;
            SetScore();
        }

        public void SetScore()
        {
            scoreText.text = $"Score : {playerScore}";
        }

        public int GetScore()
        {
            return playerScore;
        }
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");

            if (Instance == null)
            {
                Instance = this;
            }
            
            if (Instance != this)
            {
                Destroy(this.gameObject);
            }
            
            DontDestroyOnLoad(Instance);
        }
        
        private void OnRestarted()
        {
            finalScoreText.gameObject.SetActive(true);
            finalScoreText.text = $"High Score : {playerScore}";
            GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(true);
            ResetScore();
            SetScore();
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}


