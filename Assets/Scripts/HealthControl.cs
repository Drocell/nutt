using Spaceship;
using UnityEngine;
using UnityEngine.UI;

public class HealthControl : MonoBehaviour
{
    [SerializeField] private Slider healthBar;
    
    public static HealthControl Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
            
        if (Instance != this)
        {
            Destroy(this.gameObject);
        }
            
        DontDestroyOnLoad(Instance);
    }

    internal void SetMaxHealth(int maxHp,int currentHp)
    {
        healthBar.maxValue = maxHp;
        healthBar.value = currentHp;
    }
    
    internal void SetHealth(int currentHp)
    {
        healthBar.value = currentHp;
    }
}
