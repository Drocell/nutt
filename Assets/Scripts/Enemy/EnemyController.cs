﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        [SerializeField] private float minimumDistance;

        [SerializeField] private Transform playerTarget;
        [SerializeField] private Vector3 delta;
        internal void Init(Transform target)
        {
            playerTarget = target;
        }
        
        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
            GetPlayerPosition();
        }

        private void GetPlayerPosition()
        {
            delta = transform.position - playerTarget.position;
        }

        private void MoveToPlayer()
        {
            transform.up = delta.normalized;
            if (delta.magnitude > minimumDistance)
            {
                if (delta.magnitude < chasingThresholdDistance)
                {
                    transform.position += -delta * Time.smoothDeltaTime;
                }
                else
                {
                    return;
                }
            }
        }
    }    
}

